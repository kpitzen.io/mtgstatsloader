variable "region" {
  default = "dev"
}

variable "tag" {
  default = "dev"
}

provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "kpitzen-ci"
    key    = "mtgstats.batch.tf"
    region = "us-east-1"
  }
}

data "aws_caller_identity" "current" {}

data "aws_ecr_repository" "repository" {
  name = "mtgstatsloader"
}

resource "aws_iam_role" "lambda_role" {
  name = "${var.region}_mtgstatsBatchLambdaRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda_policy" {
  name = "${var.region}_mtgstatsBatchLambdaPolicy"
  role = "${aws_iam_role.lambda_role.id}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ecs:RunTask",
        "iam:PassRole"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
POLICY
}

resource "aws_cloudwatch_event_rule" "schedule" {
  name_prefix         = "mtgstats_batch_lambda_sch"
  schedule_expression = "cron(0 8 1 * ? *)"
  description         = "Schedule to run jobs at 3:00 AM CST. The first of the month"
}

resource "aws_cloudwatch_event_target" "event" {
  rule = "${aws_cloudwatch_event_rule.schedule.name}"
  arn  = "${aws_lambda_function.batch_runner_fn.arn}"
}

resource "aws_lambda_permission" "lambda_cloudwatch_permission" {
  statement_id  = "${var.region}_AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.batch_runner_fn.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.schedule.arn}"
}

resource "aws_lambda_function" "batch_runner_fn" {
  filename         = "mtgstats-batch-runner.zip"
  function_name    = "${var.region}-mtgstats-batch-runner"
  role             = "${aws_iam_role.lambda_role.arn}"
  handler          = "run_batch.handler"
  source_code_hash = "${base64sha256(file("mtgstats-batch-runner.zip"))}"
  runtime          = "python3.6"

  environment {
    variables = {
      REGION = "${var.region}"
    }
  }
}

data "aws_iam_policy_document" "task_service_assume_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com", "ecs.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "ecs_service_policy" {
  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
  }
}

resource "aws_iam_role" "task_role" {
  name = "${var.region}AwsEcsTaskExecutionRole"

  assume_role_policy = "${data.aws_iam_policy_document.task_service_assume_policy.json}"
}

resource "aws_iam_role_policy" "task_role_policy" {
  name = "${var.region}AwsEcsTaskRole"
  role = "${aws_iam_role.task_role.id}"

  policy = "${data.aws_iam_policy_document.ecs_service_policy.json}"
}

resource "aws_s3_bucket" "glue_bucket" {
  bucket = "${var.region}.mtgstats.card.data"
  acl    = "private"

  tags {
    Name        = "mtgstats.net"
    Environment = "${var.region}"
  }
}

resource "aws_s3_bucket" "rds_bucket" {
  bucket = "${var.region}.mtgstats.rds.backup"
  acl    = "private"

  tags {
    Name        = "mtgstats.net"
    Environment = "${var.region}"
  }
}

data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_instance_role" {
  name = "${var.region}MtgstatsEcsRole"

  assume_role_policy = "${data.aws_iam_policy_document.instance-assume-role-policy.json}"
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role" {
  role       = "${aws_iam_role.ecs_instance_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_cloudwatch_log_group" "ecs_log_group" {
  name = "${var.region}-mtgstats-batch-logs"
}

resource "aws_ecs_cluster" "cluster" {
  name = "${var.region}-mtgstats-batch-cluster"
}

resource "aws_ecs_task_definition" "task" {
  family = "${var.region}-mtgstats-batch-task"

  container_definitions = <<JSON
        [
            {
                "name": "mtgstatsloader",
                "image": "${data.aws_ecr_repository.repository.repository_url}:${var.tag}",
                "cpu": 256,
                "memory": 1024,
                "essential": true,
                "logConfiguration": {
                    "logDriver": "awslogs",
                    "options": {
                        "awslogs-region": "us-east-1",
                        "awslogs-group": "${aws_cloudwatch_log_group.ecs_log_group.name}",
                        "awslogs-stream-prefix": "mtgstats-batch"
                    }
                }
            }
        ]
        JSON

  cpu                      = 256
  memory                   = 1024
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  execution_role_arn       = "${aws_iam_role.ecs_instance_role.arn}"
  task_role_arn            = "${aws_iam_role.task_role.arn}"
}

resource "aws_s3_bucket" "logs" {
  bucket = "${var.region}.mtgstats.batch.logs"
  acl    = "private"

  force_destroy = true

  policy = <<POLICY
{
  "Id": "Policy1528812304073",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1528812301639",
      "Action": [
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${var.region}.mtgstats.batch.logs/*",
      "Principal": {
        "Service": [
          "ecs.amazonaws.com"
        ]
      }
    }
  ]
}
POLICY
}

resource "aws_dynamodb_table" "table" {
  name           = "${var.region}-card-data"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "name"
  range_key      = "set"

  attribute {
    name = "name"
    type = "S"
  }

  attribute {
    name = "set"
    type = "S"
  }
}

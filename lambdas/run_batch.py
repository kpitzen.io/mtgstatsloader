import boto3
import os

taskDefinition = '{}-mtgstats-batch-task'.format(os.environ['REGION'])
cluster = '{}-mtgstats-batch-cluster'.format(os.environ['REGION'])
count = 1
launchType = 'FARGATE'
networkConfiguration = {
    'awsvpcConfiguration': {
    'subnets': [
        'subnet-d8117ad7',
        'subnet-d57fc9fb',
        'subnet-dd639ae3',
        'subnet-044f344e',
        'subnet-036ac064',
        'subnet-65a41039'
    ],
    'securityGroups': ['sg-a01cd3ea'],
    'assignPublicIp': 'ENABLED'
    }
}

def handler(event, context):
    ecs = boto3.client('ecs')
    response = ecs.run_task(
        cluster=cluster,
        taskDefinition=taskDefinition,
        count=count,
        launchType=launchType,
        networkConfiguration=networkConfiguration
    )
    print(response)
    return {
        "response": "Running task!"
    }
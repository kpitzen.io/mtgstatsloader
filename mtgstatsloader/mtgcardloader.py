import mtgsdk
import pickle
import json
import boto3
import os
import decimal
import re

def replace_decimals(obj):
    if isinstance(obj, list):
        for i in range(len(obj)):
            obj[i] = replace_decimals(obj[i])
        return obj
    elif isinstance(obj, dict):
        for k in obj:
            obj[k] = replace_decimals(obj[k])
        return obj
    elif isinstance(obj, float):
        if obj % 1 == 0:
            return int(obj)
        else:
            return decimal.Decimal(obj)
    else:
        return obj

class MTGCardLoader:

    def __init__(self, bucket_name: str, logger, card_file: bool=True):
        self.log=logger
        self.cards = []
        self.bucket_name = bucket_name
        self.__output_cards = None
        self.__card_file = card_file
        self.__card_filename = 'mtgstats.card.data'
        if card_file:
            self.load_cards()
        else:
            self.fetch_cards()

    def _update_output_cards(self):
        self.__output_cards = '\n'.join([json.dumps(vars(card)) for card in self.cards])

    def load_cards(self):
        if self.__card_file:
            self.log.info('Loading Cards from File')
            with open('cards.bin', 'rb') as card_file:
                self.cards = pickle.load(card_file)
                self._update_output_cards()

    def save_cards(self):
        self.log.info('Saving cards to file')
        with open('{}'.format(self.__card_filename), 'w') as card_output:
            self._update_output_cards()
            print(self.__output_cards, file=card_output)

    def upload_cards(self):
        self.save_cards()
        if self.__output_cards:
            self.log.info('Uploading Cards to S3')
            with open('{}'.format(self.__card_filename), 'rb') as card_file:
                s3 = boto3.client('s3')
                response = s3.upload_fileobj(
                    Fileobj=card_file,
                    Bucket=self.bucket_name,
                    Key=self.__card_filename
                )
                return response

    def fetch_cards(self):
        self.log.info('Getting Cards from API')
        with open('cards.bin', 'wb') as card_file:
            self.cards = mtgsdk.Card.all()
            self._update_output_cards()
            pickle.dump(self.cards, card_file)

    def load_to_dynamo(self):
        self.log.info('Loading cards into dynamodb')
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('{}-card-data'.format(os.environ['REGION']))
        processed_keys = set()
        with table.batch_writer() as batch:
            for card in self.cards:
                output_dict = {}
                card_vars = vars(card)
                for key in card_vars:
                    if card_vars[key]:
                        output_dict[key] = card_vars[key]
                card_item = replace_decimals(output_dict)
                card_item['name'] = re.sub(r'[\W]', '', card_item['name'].lower())
                try:
                    assert card_item['name']
                except AssertionError:
                    self.log.warning('Malformed card data: {}'.format(card_item))
                    continue
                card_item['displayName'] = card.name
                if (card_item['name'], card_item['set']) not in processed_keys:
                    batch.put_item(
                        Item=card_item
                    )
                    processed_keys.add((card_item['name'], card_item['set']))

import mtgcardloader
import argparse
import logging
import os

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('-f', '--card-file', action='store_true', help='When set to false, downloads all current MTG cards from API')
arg_parser.add_argument('-s', '--save-cards', action='store_true', help='Save cards to disk')

args = arg_parser.parse_args()

if __name__ == '__main__':
    FORMAT = "%(asctime)s:%(levelname)s:%(name)s.%(funcName)s:%(message)s"
    logging.basicConfig(level='INFO', format=FORMAT)
    logger = logging.getLogger('mtgstatsloader')
    s3_bucket = None
    try:
        s3_bucket = '{region}.mtgstats.card.data'.format(region=os.environ['REGION'])
    except:
        logger.warning('No environment set.  Will not upload to s3.')
    card_loader = mtgcardloader.MTGCardLoader(bucket_name=s3_bucket, logger=logger, card_file=args.card_file)
    if args.save_cards:
        card_loader.save_cards()
    if s3_bucket:
        s3_response = card_loader.upload_cards()
        card_loader.load_to_dynamo()
        print(s3_response)
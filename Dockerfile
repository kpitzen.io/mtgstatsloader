FROM python:slim

ARG BUILD_REGION=dev
ARG BUILD_AWS_ACCESS_KEY_ID=None
ARG BUILD_AWS_SECRET_ACCESS_KEY=None

ENV REGION=$BUILD_REGION
ENV AWS_ACCESS_KEY_ID=$BUILD_AWS_ACCESS_KEY_ID
ENV AWS_SECRET_ACCESS_KEY=$BUILD_AWS_SECRET_ACCESS_KEY

ENV ECS_AVAILABLE_LOGGING_DRIVERS='["json-file","awslogs"]'

ADD . /usr/sbin/

RUN ls -al /usr/sbin/

RUN pip install -r /usr/sbin/requirements.txt

ENTRYPOINT [ "python", "/usr/sbin/mtgstatsloader" ]